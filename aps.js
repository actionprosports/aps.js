// Created by: Daniel Westendor
// Copywrite Action Pro Sports
// www.actionprosports.com

window.APS = (function(config) {
  var _ajax = function(url, method, success, failure) {
    // No support for OOLLLLLDD IE
    var _http = new XMLHttpRequest();

    _http.onreadystatechange = function() {
        if (_http.readyState == 4){
          // Completed

          if (_http.status > 399)
            failure(_http.responseText, _http.status); // Blanket error responses
          else
            success(JSON.parse(_http.responseText), _http.status)
        }
    }

    _http.open(method, url + '.json', true);
    _http.send();

  }

  var options = {
    errorTemplate: '<div class="error">{{ error }}</div>',
    productTemplate: '<a href="{{referral_url}}" target="_blank" class="product" itemscope itemtype="http://schema.org/Product">\
                        <h1 class="name" itemprop="name">{{ name }}</h1>\
                        <img itemprop="image" src="{{medium_image.url}}"/>\
                        <p itemprop="offers" itemscope itemtype="http://schema.org/Offer">\
                          <span itemprop="price" class="price">{{display_price}}</span>\
                        </p>\
                      </a>'
  };

  _.extend(options, config);
  _.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
  };

  return {
    products: function(target, callback) {
      var callback = callback || _.noop;
      var targetEl = document.querySelector(target);

      if (targetEl == null) throw "Target not found";

      _ajax(options.userUrl, 'GET', function success(response, status) {
        var result = '';
        var compiledProduct = _.template(options.productTemplate);
        for (var i = 0; i < response.products.length; i++) {
          result += compiledProduct(response.products[i]);
        };

        targetEl.innerHTML = result;
        callback('success');
      }, function failure(error, status) {
        var compiledError = _.template(options.errorTemplate);
        targetEl.innerHTML = compiledError({error: error});
        callback('error');
      });
    }
  }
})